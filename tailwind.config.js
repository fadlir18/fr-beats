module.exports = {
  content: ['./index.html'],
  darkMode: 'class',
  theme: {
    fontFamily: {
      'titillium': ['Titillium Web'],
    },
    container:{
      center: true,
      padding: '16px',
    },
    extend: {
      colors: {
        primary: '#f59e0b',
        secondary: '#64748b',
      },
      screens: {
        '2xl': '1120px',
        'xl': '1000px',
      }
    },
  },
  plugins: [],
}
